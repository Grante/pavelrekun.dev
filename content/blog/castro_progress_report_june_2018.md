---
title: "Castro Progress Report: June 2018"
date: 2018-06-18
draft: false
---

This month, Castro receives 2.8 update, which makes great changes in methods of obtaining information for greater accuracy and precision, adds a new category of Bluetooth and much more!

## Information

### Bluetooth

The most significant change is the addition of a new information category — Bluetooth.

![](https://cdn-images-1.medium.com/max/2064/1*bRdSTENHq_v6LJf99Y_fJg.jpeg)

Now you can check the basic settings for Bluetooth of your device, check the support of “Low Energy” technology and get data about all connected devices.

### Identifiers

You have been asking to add the Google Services Framework ID and now it is available in the category Device.

Also support for two Sim devices was increased by adding IMEI 2.

Added support of MEID and MEID 2 for CDMA devices.

### Wi-Fi

Signal quality is now displayed in percentages, for a more convenient understanding.

### System

The second function, according to the frequency of requests for addition, is the version of Google Play Services.

This information is now available in the other miscellaneous. Java Runtime version was added too.

### Device

Support for HDR displays has been added. If you are a welder with such display, you can check the maximum, average and minimum brightness range.

### Processor

Improved support for 64-bit CPUs in Supported ABI field.

The database of devices and processor updated with new information:

* OnePlus 6, Xiaomi (MI 6X, MI 8, MI 8 SE, Redmi S2), BlackBerry KEY2, Nokia (3.1, 5.1, 6.1, 8 Sirocco, X6), LG V35 ThinQ, Meizu (15, 15 Lite, 15 Plus, E3), Primo N2

* Snapdragon 660, Snapdragon 710

## Design

Many items have been updated in accordance with the Material Design 2. All icons have been updated with new optimized versions.

{{< figure src="https://cdn-images-1.medium.com/max/3208/1*ks5I2PkH9AZqs19nDgFwWw.jpeg" title="Left: Castro 2.7 | Right: Castro 2.8" >}}

I also worked on the fonts in the application, now all the headings use the font of Robot Mono. This font accents itself much better than default fonts.

## Features

To avoid erroneous closing of the application, I have added ability to close the application only by double-clicking the back button. In the settings, the ability to reset all measurement settings to the default state has been added. From the SIM category, you can now go to the SIM card’s system settings. Also, the basic support of Android P was added, which should be released later this year. Full support will be added closer to the release of the update.

## Fixes

* [Wi-Fi]: In properties section SSID field will be hidden if Hidden SSID enabled in router.

* [Codecs]: All codecs types were marked as “Unavailable”.

* [System]: Bootloader version was marked as “Unknown”.

* [System]: Wrong detection of SELinux state.

* Useless restart dialogs in measurement settings removed.
