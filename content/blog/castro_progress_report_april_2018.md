---
title: "Castro Progress Report: April 2018"
date: 2018-04-12
draft: false
---

This month Castro got many improvements, new Premium features, Lineage OS SDK support and bug fixes. Let’s discover the update in a more detailed way.

Update already available in Google Play ([Basic](https://play.google.com/store/apps/details?id=com.itemstudio.castro) and [Premium](https://play.google.com/store/apps/details?id=com.itemstudio.castro.pro)) and [XDA Labs](https://labs.xda-developers.com/store/app/com.itemstudio.castro).

## Premium

In this update Premium users will get one more Night mode feature and fully reworked widget.

### Widget

Widget in Castro 2.7 was fully reworked, starting from configure screen and ending widget design. Configure screen was updated with a new Monospace font for titles and one more refresh interval option — 10 seconds. Widget preview now uses real-time data instead of hardcoded values.

{{< figure src="https://cdn-images-1.medium.com/max/4380/1*dtKHxFaF-y2RqJ8QqZPsbw.jpeg" title="Left: Castro 2.6 | Right: Castro 2.7" >}}

Widget’s layout was also updated with Monospace font for titles, Material Design specs for margins and proper information.

{{< figure src="https://cdn-images-1.medium.com/max/4380/1*fOkqDRAs3fj7Q30g3aqEuA.jpeg" title="Left: Castro 2.6 | Right: Castro 2.7" >}}

Most of the work was done with the internal logic of the widget. In past versions its work was not very stable. The widget constantly stopped working, the background color was lost and so on. The problem was especially acute on the new versions of Android. It was decided to completely redesign the background update mechanism for the widget. Now the widget itself understands when it is necessary to turn off the information update to save energy (for example, when the screen is off). Also, the widget will automatically restart within a minute after the device reboots. All this will work equally well starting with Android Lollipop and ending with Android Oreo.

### New night mode - AMOLED

A great upgrade for premium users is the new night mode. AMOLED Night Mode is best suited for devices with OLED/AMOLED screens. Since this type of screen does not consume energy when displaying a completely black color, a new theme allows you to save energy on devices with such screens.

{{< figure src="https://cdn-images-1.medium.com/max/6560/1*DkhTpemELnAxE3OM5uRvJg.jpeg" title="Day | Night | Night (AMOLED)" >}}

## Information

Support for Lineage OS was added in this update. Lineage OS users can now check for LOS version. In Battery section we have added support for performance profiles usage for Lineage OS users. Also current charge speed information method updated to work on more devices.

Added support for Kirin 659 processor. Lenovo YB1-X90F, Huawei P20, P20 PRO, P20 Lite and Xiaomi Mi MIX 2S added to devices database.

## Design

More colors combinations were added in Castro 2.7. Now you can more accurately choose the style of the application that suits you best. A large number of colors are available from completely white to bright yellow.

The new theme engine allows automatically change the color of the text from white to black depending on the color you choose in places where it’s really needed. This makes it possible to improve the visibility of many elements.

The dialog for color selection has also been redesigned. The correct Material Design specifications were used. Now you can see what color is being used at the moment.

{{< figure src="https://cdn-images-1.medium.com/max/6560/1*DkhTpemELnAxE3OM5uRvJg.jpeg" title="Left: Castro 2.6 | Right: Castro 2.7" >}}

## Other

Alongside with the configure widget screen update and the new Color Dialog Castro got updated Codec Dialog layout with support for dynamic colors change.

Other small changes and bug fixes listed below:

* Improved: Permissions handling in SIM section.

* Removed: Support for DashClock widgets.

* Updated: Information handler for Codecs section.

* Fixed [PREMIUM]: Configure widget screen’s missing background in rare cases.

* Fixed [PREMIUM]: Memory leak during widget’s background work.

* Fixed [PREMIUM]: Configure widget screen now properly works with Night modes.

* Fixed [PREMIUM]: Configure widget screen now properly works with accent colors.

* Fixed [PREMIUM]: Crashes when trying to open system settings from information sections.

* Fixed: Mistakes in localization for Device section.

* Fixed: Missing title in Measurements settings.

* Fixed: Crash when trying to open Google Play from application.

* Fixed: Missing launcher icon on Android Nougat and earlier.
