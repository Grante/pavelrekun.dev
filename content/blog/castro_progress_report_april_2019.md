---
title: "Castro Progress Report: April 2019"
date: 2019-04-12
draft: false
---

Moving on, after the release of a large update 3.0 a month later, I release the next update - 3.1, which contains even more information, updates for Premium users and event more features. Let's start!

[Castro](https://play.google.com/store/apps/details?id=com.itemstudio.castro) | [Castro Premium](https://play.google.com/store/apps/details?id=com.itemstudio.castro.pro)

[Detailed changelog](https://pavelrekun.dev/castro/changelog_release/)

## Features

### General
Many users asked to revert one feature from Castro 2.0, called **System settings**, which made it possible to open the system settings directly from the categories in the program. This function returns in this update.

{{< figure src="https://i.imgur.com/VWztVPU.jpg" title="Memory's settings" >}}

Also, I am finally enabling support for **Cloud Messaging** in this update, this means that users will be notified about any important news, sale promotions or any other updates via notifications. I am not going make ads from this, just to inform users.

You can save graphs states in Sensor details screen in Gallery for later usage.

{{< figure src="https://i.imgur.com/Dwo5ilA.jpg" title="Save button" >}}
{{< figure src="https://i.imgur.com/XfIJDgB.jpg" title="Result" >}}

Added new screen - **Applications hub**. It's contains information about all my applications and currently there is my new application in beta - Skit, check it out.

Also, all links in application's settings updated from my public GitHub repository to my new personal site and you are reading this blog on it already.

### Tools

**Screen tester** tool gets new customization option - **Coordinates**. This option will draw coordinates of every touch on the screen, which will gave even more information about your device's screen.

{{< figure src="https://i.imgur.com/Ijyu0Fp.jpg" title="Coordinates" >}}

There are also some improvements for Premium users. I have added new power saving mode for **Speed monitor**, which will turn notification off, when battery of the device will be low. This will help save a little more charge when needed. Also, click on notification will redirect you to configuration page now.

{{< figure src="https://i.imgur.com/zTgPc7R.jpg" title="Battery saver" >}}

## Information
In this update I am adding long awaited information category - DRM. It will contain information about **Widevine DRM** and **PlayReady DRM** with **ClearKey DRM** in future. Castro will provide information about vendor, version, algorithms, security level and even more.

{{< figure src="https://i.imgur.com/AqX4e9E.jpg" title="DRM" >}}

System category also getting some update, it will now have new sub-category - **Virtual machine**, which contains information about virtual machine, runtime environment and totaly new information - Heap sizes. It is special memory allocated for virtual machine of your device.

## Design
There are not many updates in design in this version. For Premium users I have added new theme - Olive, which is part of dark themes pack. Thanks to the customization system introduced in the last update, I am going to release new themes (light and dark) in each update.

{{< figure src="https://i.imgur.com/hSYbXzU.jpg" title="New theme - Olive" >}}

## What’s next?
This and probably next month I will be working more on Skit, I want to move it in release really soon and then Castro and Skit will be updated by turns. If you have any ideas or suggestions for improving my applications, then I am always ready to listen to you on **pavel.rekun.development@gmail.com**.
