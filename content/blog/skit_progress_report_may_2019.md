---
title: "Skit Progress Report: May 2019"
date: 2019-05-15
draft: false
---

After the very successful launch of Skit in the past month, the first major update for it comes out - **1.1**, which contains search for applications, activities launching and even more information about applications! But first things first.

As always, you can download basic version or buy Premium visiting links below:

[Skit](https://play.google.com/store/apps/details?id=com.pavelrekun.skit) | [Skit Premium](https://play.google.com/store/apps/details?id=com.pavelrekun.skit.premium)

[Detailed changelog](https://pavelrekun.dev/skit/changelog_release/)

## Functionality

Search is probably the most frequent feature that users have asked to add, and it is finally here, there is a search for user and system applications that can be launched by clicking on the "Magnifier" button on both screens. The search is made in the form of a separate dialog and input field. As soon as you start typing name of application you are interested in, the filter will immediately start working and show you all the relevant results. It is worth noting that the multi-select mode in the Premium application and the search so far can not work simultaneously.

{{< figure src="https://i.imgur.com/Dx4R1lG.jpg" title="Search results" >}}

The second most frequent request feature is the launch of certain application activities. Skit can run only those activities that have this capability (a special flag is set when developing an application). To start, you just need to select the activity that interests you, and if it is possible to start it, you will be able to click the "Start" button.

{{< figure src="https://i.imgur.com/XkUAyBn.jpg" title="Activity details with ability to launch it" >}}

Some enthusiasts and developers have already started using the new version of Android Q (which is still not released by Google) on an ongoing basis and until this point Skit could not correctly determine the target and minimum OS versions on the details and statistics screens. Now there is such an opportunity, although I want to note that full support for the new version of Android Q will be added only after the release of the final version of the Q APIs.

{{< figure src="https://i.imgur.com/kIrbNfY.png" title="Small part of Q support :)" >}}

## Information

More specific permissions have been added, namely: Restart packages, Use credentials, Read logs, Authenticate accounts, Package usage stats, Manage accounts and Manage documents. I'm still working on introducing even more permissions definers and will continue to work on this.

In addition, a special method for handling custom permissions was added, namely, Firebase and Google permissions.

In the details of the application, a new category of information has been added - Directories. It accommodates both the old data: path to APK and application's data and the new one: path to native libraries and protected data. And in the details of the application’s signature certificate, information was added about the state and city of issuance of the certificate.

{{< figure src="https://i.imgur.com/2wDvjLD.jpg" title="Directories" >}}

## Other

There are also some smaller improvements and fixes:

* Added: Premium version of application now has updated launcher icon.
* Improved: Overhaul stability of application.
* Improved: Tutorial on Applications screen removed, but some of steps moved to Onboard screen.
* Improved: Premium icon's "plus" color to general style in Other screen.
* Improved: Applications hub icons updated.
* Improved: Loading process of Details screen.
* Improved: Configs for Firebase logging now supports Premium version.
* Fixed: Permissions list could be empty for some applications.
* Fixed: Some statistics chart where hidden by default.

**That's all for now, if you have additional ideas for improving the application or you want to see some new features for the application, then I am always available at: pavel.rekun.development@gmail.com**
