---
title: "Castro Progress Report: March 2019"
date: 2019-03-15
draft: false
---

So, the time has come, after a long break from big updates Castro receives the largest and most ambitious update ever — 3.0. In this update Castro has completely redesigned everything from design and navigation to the available information. You can already download update visiting the links below:

[Castro](https://play.google.com/store/apps/details?id=com.itemstudio.castro) | [Castro Premium](https://play.google.com/store/apps/details?id=com.itemstudio.castro.pro)

[Detailed changelog](https://pavelrekun.dev/castro/changelog_release/)

Let’s get started!

## Design

I will not say much, the application was completely redesigned. All screens and whole navigation was recreated from scratch. Castro (and all my future apps) is using own Material Theming styles (which are looking clean and accurate). Screens are no longer overloaded with information and graphs, but all data is displayed in a comfortable for eyes style. But enough words, let’s watch.

{{< figure src="https://cdn-images-1.medium.com/max/4566/1*hUc9xzmRkhzmaU8aV9tq7A.png" title="Information" >}}

{{< figure src="https://cdn-images-1.medium.com/max/4566/1*4C2K-twJOlM-E99b7fm3BA.png" title="Navigation" >}}

{{< figure src="https://cdn-images-1.medium.com/max/4566/1*wo7Qc0zDg0g2lU3HmGubZA.png" title="Feedback" >}}

{{< figure src="https://cdn-images-1.medium.com/max/4566/1*-ZUYQzY5mGdyaQn3CBx_Nw.png" title="About" >}}

Also, for lovers of customization of the user interface the selection of colors has been significantly expanded (from 20 to 80).

![](https://cdn-images-1.medium.com/max/2160/1*a7Agu7wBVs9KObc2lnUgqA.jpeg)

## Features

Starting with this release, Castro changes the development vector from “Information only” to “Information + diagnostics”. What does it mean? Well, starting from this release Castro new and new diagnostic tools for the device state will be added. In this update this is a redesigned export capability and screen tester. But let’s order.

### Export

This functionality has been completely redesigned. Now it takes up its own screen. Now you have the opportunity to fully hide the anonymous and dynamic information from the created report. And now you or the person you want to share your report with will be notified about this in the report itself.

![](https://cdn-images-1.medium.com/max/2160/1*HNqLo3R-4DMczWRIufnRiQ.jpeg)

In addition, after a report is successfully created you will be given the opportunity to immediately open and view it or send it in any convenient way.

The appearance of the document in the .PDF format has also been completely redesigned, which will now be available only to Premium users.

### Screen tester

And this is a completely new feature for Castro. With it, you can check your display for the presence of dead pixels, screen highlights and multi-touch capabilities.

![](https://cdn-images-1.medium.com/max/2160/1*18rRNw4REkJ9V7Pw7THt7g.jpeg)

You can customize all the work of this function, starting from the colors of the touch display to the intensity of vibration.

## Information

The main update for me personally is a new engine for receiving and displaying information. Now, to receive information, add it to the screen and collect it for exporting, you only need 1 step, instead of 3. But these are all changes under the hood that will not be noticeable to the user, let’s talk about the new data sets.

### Camera

The largest update information received category cameras. Previously, we had two separate categories for the front and rear cameras, and now they are combined into one. Now information is completely based on Camera2 API. This means that Castro can now show you more information and it will be more relevant. New information contains: physical size, color filter, focus range, aperture, OIS support, ISO range and much more!

### Other

* Identifiers now separated into separate category;

* Improved detection of all types of memory usages;

* Supported ABIs in Processor category now separated in 32-Bit and 64-Bit;

* Calculation of frequency range improved for processors with big.LITTLE architecture;

* Root now has version in System category;

* Added support for Treble and Treble (A/B) checks;

* Aspect ratio of the display in Device category;

* Added new devices in DB: Samsung (Galaxy S10, Galaxy S10 Plus, Galaxy S10e, Galaxy A10, Galaxy A30, Galaxy A50, Galaxy M30), Xiaomi (Redmi GO, MI 9, MI 9 SE), Nokia 1 Plus, Sony (Xperia 10, Xperia 10 Plus, Xperia L3).

* And much more!

## Premium

I saved something new for Premium users, namely: a new customization system and an updated report design in .PDF format.

### Theme engine

Before Castro offered you a choice only between night mode and day mode. Now, however, the new themes engine allows you to choose between a set of themes, namely: White, Sepia, Dark, Dark (Blue) and Black.

![](https://cdn-images-1.medium.com/max/10800/1*vhMocQ5GY-5NN-WtlaEzEw.png)

The system has become more dynamic and convenient as well as the opportunity to preview themes during the selection.

![](https://cdn-images-1.medium.com/max/2160/1*lJe344jcCX60RQbMGFiPng.jpeg)

The system is now more susceptible to expansion, so you can expect more topics in future updates.

### Reports in .PDF

Also, the ability to create reports in .PDF format is now available only to premium users. The design of the document has been completely updated with new fonts and category delimiters.

![](https://cdn-images-1.medium.com/max/2000/1*zlwBO-bew7dwzfz6a8V67g.png)

## What’s next?

Thanks to completely redesigned data management systems releasing new updates should be much easier, so you can expect more frequent updates. If you have any suggestions or you have found some kind of defect in the application, please email me: **pavel.rekun.development@gmail.сom** and I will try to help you! I also work hard on the site for all my projects and blogs to transfer all the information (lists of changes, privacy policy, etc.) to one place.

I have some new projects in stock that will help organize the system of tools for Android along with Castro and one of the projects is almost ready for the beta testing stage, so expect news in the near future.

I hope you enjoy this update just like I do!
