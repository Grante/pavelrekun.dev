---
title: "Meet the new family member - Skit"
date: 2019-04-23
draft: false
---

Well, it's time to introduce you to a new member so far not a large family of my applications. The new application is called Skit and it is designed to help you manage all your installed and system applications on your device running Android OS. But let's look at everything in more detail.

[Skit](https://play.google.com/store/apps/details?id=com.pavelrekun.skit) | [Skit Premium](https://play.google.com/store/apps/details?id=com.pavelrekun.skit.premium)

## Features
Skit is created for two categories of people: regular users and enthusiasts / developers. Users can manage their applications and learn more about them. But enthusiasts and developers get access to a deeper item of applications, such as lists of activities, services, and even a certificate of application signature.

{{< figure src="https://i.imgur.com/9jRZOjb.png" title="Applications" >}}

Also, the main function is the ability to manage installed and system applications, namely: extracting APK, further sending it to your contacts, searching for an application in "Google Play" and some other functions.

{{< figure src="https://i.imgur.com/tGDmTf1.png" title="Actions" >}}

A separate function that I would like to highlight is the application permissions identifier. I wrote a huge system that can determine 95% of the basic application permissions. Skit separates all permissions into two categories: defined (full information access) and undefined (only system name defined).

{{< figure src="https://i.imgur.com/uihg34i.png" title="Permissions" >}}

Skit uses a slightly modified skeleton design from Castro (which was designed for use in all new and future applications). Therefore, from the start, Skit can provide access to all customization kits available in Castro.

## Premium
Skit also using the same monetizing scheme as Castro: Basic and Premium version. Basic version has no annoying ads and provides a basic set of functionality. But the Premium version has a set of additional functions, such as: customization of the themes, detailed statistics about all the applications on the device and batch delete and extract operations for user applications. I would like to highlight the last two functions.

Detailed statistics includes compiling applications according to certain criteria (minimum required OS, target OS and installation location) and forms them in the form of beautiful graphs, on which sectors you can click to see a set of applications.

{{< figure src="https://i.imgur.com/WkXY76L.png" title="Statistics" >}}

The second large Premium feature is batch operations on installed applications. You can enable sampling mode simply by clicking on the application icon in the list or holding the list item, and then simply click on the applications you want to select. In this mode, you can extract or delete several applications at once.

{{< figure src="https://i.imgur.com/xP6h9lE.png" title="Batch operations" >}}

## What's next?
I do not plan to stop updating both of my programs and in the future I plan to submit updates for them one by one. In the next couple of weeks, I plan to send all my strength to correct defects found and high-quality user support.
