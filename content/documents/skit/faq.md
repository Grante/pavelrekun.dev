---
title: "FAQ"
draft: false
url: "/skit/faq/"
---

## Basics
### What are the system requirements of the program?
Android 5.0 (API 21) and above.

### Why are there two Skit applications?
There are two versions of Skit - Basic and Premium. Premium version has extra features, that doesn't available in basic version. The Premium version of Skit is a standalone application.

### What is the difference between Basic and Premium version?
The Premium version enables such features:

* Theme customization support

* Batch applications operations

* Detailed programs statistics

I am developing new Premium features for every update. Buy once, get it all!

### What will happen if I will change my device?
When you purchase Skit Premium, in Google Play, it will be assigned to your Google account. If you use the same Google account on a new or factory reset device, you will have access to all previous purchases.
