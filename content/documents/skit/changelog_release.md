---
title: "Changelog (Release)"
draft: false
url: "/skit/changelog_release/"
---

### Current versions

**Release** - 1.1.2

**Beta** - Stopped

## Skit [1.1.2]
### Localization
- Added: Even more localizations for you.

## Skit [1.1.1]
### Features
- Added: Crash protection when activity can't be launched.

### Fixes
- Fixed: Activity details dialog should be auto closed after launching.
- Fixed: Crash during hiding/showing search button on sliding fragments.

## Skit [1.1]
### Features
- Added: Search dialog for user and system applications.
- Added: Ability to launch specific activities, if it is possible.
- Added: Very initial support for Android Q (for Analytics, Information and Statistics screens).
- Added: Premium states for Onboard and About screens.
- Updated **[PREMIUM]**: Tutorial on Applications screen removed and some of steps moved to Onboard screen.
- Improved: Loading process of Details screen.
- Improved: Configs for Firebase logging now supports Premium version.

### Design
- Improved: Premium icon's "plus" color to general style in Other screen.
- Improved: Applications hub icons updated.

### Information
- Added: Definer for Google permissions (such as, push-messaging and other).
- Added: Even more permissions definers - Restart packages, Use credentials, Read logs, Authenticate accounts, Package usage stats, Manage accounts and Manage documents.
- Added: New category in Application details screen - Folders, which contains new information about protected parts of application's data and native libraries.
- Added: Certificate information now contains additional information about Subject and Published - City and State of creation.

### Fixes
- Fixed: Sometimes permissions list was empty.
- Fixed: Some statistics chart where hidden by default.
- Fixed: Overhaul stability of application improved.

## Skit [1.0.8]
### Features
- Improved [PREMIUM]: Tutorial can be properly canceled now.

### Fixes
- Fixed: Analytics screen loading process.

## Skit [1.0.7]
### Features
- Added: Ability to find system applications in "Google Play".
- Improved: Feedback subject generation.

### Design
- Improved: Padding and margins at About and Premium screens.

### Localization
- Added: Italian language.
- Added: Portugal (Brazil) language.

### Fixes
- Fixed: Application details screen validation.
- Fixed: Application's icon memory management.
- Fixed: Analytics screen loading process.

## Skit [1.0.6]
### Features
- Improved: Analytics measurement process.
- Improved: Applications details availability checks.

## Skit [1.0.5]
### Features
- Improved: Application deleting process.
- Improved: Removed empty "Minimum OS" container in Statistics screen for Android M and older.
- Improved: Application crashes catching.

### Fixes
- Fixed: Wrong color of "Foreground service" permission icon.
- Fixed: Components buttons shifted text for small screens.

## Skit [1.0.4]
### Fixes
- Fixed: Blank "General" category in Settings screen for Basic version.

## Skit [1.0.3]
### Features
- Added [PREMIUM]: Minimal and Target OS statistics should display OS version instead of SDK now.
- Improved: Optimized applications loading process.

### Fixes
- Fixed: Application could hang while loading applications for too long.

## Skit [1.0.2]
### Features
- Updated: Libraries in open-source libraries dialog.

### Fixes
- Fixed: More crashes in Details screen.
- Fixed: Missing application's icon in Details screen.

## Skit [1.0.1]
### Fixes
- Fixed: Crash on devices with Android Marshmallow.
- Fixed: Crash in Details screen.

## Skit [1.0]

* Initial release.
