---
title: "Changelog (Beta)"
draft: false
url: "/skit/changelog_beta/"
---

### Current versions

**Release** - 1.1.2

**Beta** - Stopped

## Skit [1.1 RC 02]
### Localization
- Added: All finished and mostly finished localization from OneSky (all unfinished localization creators has one more day to finish it).

## Skit [1.1 RC 01]
### Features
- Added: Premium states for Onboard and About screens.
- Removed: Unused tutorial settings.
- Removed: Unused texts.
- Improved: Loading process of Details screen.
- Improved: Configs for Firebase logging now supports Premium version.

### Design
- Improved: Premium icon's "plus" color to general style in Other screen.
- Improved: Applications hub icons updated.

### Fixes
- Fixed: Some design regressions from previous builds.

## Skit [1.1 Beta 02]
### Features
- Improved: Search dialog will auto close after selecting application.

### Fixes
- Fixed: Crash while selecting application in search dialog.
- Fixed: Missing texts added to OneSky and available for localization.
- Fixed: Some statistics chart where hidden by default.
- Fixed: Overhaul stability of application improved.

## Skit [1.1 Beta 01]
### Features
- Added **[WIP]**: Search dialog for installed and system applications.
- Added **[WIP]**: Ability to launch specific activities, if it is possible.
- Added: Very initial support for Android Q (for Analytics, Information and Statistics screens).
- Updated **[PREMIUM]**: Tutorial on Applications screen removed and some of steps moved to Onboard screen.

### Information
- Added: Definer for Google permissions (such as, push-messaging and other).
- Added: Even more permissions definers - Restart packages, Use credentials, Read logs, Authenticate accounts, Package usage stats, Manage accounts and Manage documents.
- Added: New category in Application details screen - Folders, which contains new information about protected parts of application's data and native libraries.
- Added: Certificate information now contains additional information about Subject and Published - City and State of creation.

### Fixes
- Fixed: Sometimes permissions list was empty.

## Skit [1.0 RC 01]
### Features
- Updated: All Premium features removed from Basic version.
- Improved: Loading process of applications.
- Improved: Firebase codebase.

## Skit [1.0 Beta 06]
### Features
- Added: Tutorial for Premium features in Applications and Statistics screens.

### Fixes
- Fixed: Crash while reset Analytics settings.

## Skit [1.0 Beta 05]
### Features
- Added: Ability to find installed application in Google Play.
- Improved: Initialization process of Statistics screen.

### Fixes
- Fixed: Broken UI of services screen.

## Skit [1.0 Beta 04]
### Features
- Added: Calculation of all applications on device in Analytics screen.
- Improved: UI of Analytics screen.
- Improved: Code and layouts optimization.

### Fixes
- Fixed: More fixes for Statistics screen.

## Skit [1.0 Beta 03]
### Fixes
- Fixed: Regression bug in Applications and Statistics screens.
- Fixed: Regression bug with animation in User applications screen.

## Skit [1.0 Beta 02]
### Features
- Improved: Reduced application size for 2 MB.
- Improved: Changed icon of Feedback screen.
- Improved: Navigation bar's color for Android Oreo and newer.

### Fixes
- Fixed: Error dialog was not closed after selecting application from Statistics screen.
- Fixed: Message and theme were swapped in Feedback screen.
- Fixed: Link to changelog in Settings screen forwarding to 404 page.
- Fixed: Crash in Statistics screen.
- Fixed: Strange colors of buttons, dialogs and other UI.

## Skit [1.0 Beta 01]

- Initial Skit release.
