---
title: "FAQ"
draft: false
url: "/castro/faq/"
---

## Basics
### What are the system requirements of the program?
Android 5.1 (API 22) and above.

### Why are there two Castro applications?
There are two versions of Castro - Basic and Premium. Premium version has extra features, that doesn't available in basic version. The Premium version of Castro is a standalone application.

### What is the difference between Basic and Premium version?
The Premium version enables such features:

* Theme customization support

* Customizable widget

* Bandwidth speed notification

* .PDF support for information export

I am developing new Premium features for every update. Buy once, get it all!

### What will happen if I will change my device?
When you purchase Castro Premium, in Google Play, it will be assigned to your Google account. If you use the same Google account on a new or factory reset device, you will have access to all previous purchases.

## Not correct information
### Screen density
Screen density calculation can be wrong. Android can not define your real DPI, because it scales it to the nearest constant (120, 240, 360, 480, or 640).

### Battery capacity
Battery capacity can only be reported for factory default batteries. If the battery was replaced with an extended capacity battery, neither Android or Castro will be able to detect the new capacity.

### Battery minimum and maximum voltage
Minimal and maximal voltage can be reported for Snapdragon device's only for now. We will add support for other device's in future.

### Camera parameters
Camera capabilities may show incorrect information if the manufacturer encoded the wrong values into the Android profile of the device.
