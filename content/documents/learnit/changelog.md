---
title: "Changelog"
draft: false
url: "/learnit/changelog/"
---

### Current version

**Release** - 2.2

## Learn IT! [2.2] - 14.04.2019
### Features
- Added: Double back to close application.
- Added: Applications hub to display information about other applications.
- Added: Link to personal site in About screen.
- Removed: Links to Medium and Facebook from About screen.
- Updated: All links changed from public GitHub repository to personal site.

### Design
- Improved: Application icon updated with "Adaptive Icon" technology.

## Learn IT! [2.1.1] - 30.04.2018
### Tools
- Updated: Gradle Build Tools to 3.1.2.
- Updated: Kotlin to 1.2.41.
- Updated: ButterKnife to Android Kotlin Extensions converted.

## Learn IT! [2.1] - 11.03.2018
### Design
- Updated: Design of Onboard screen.
- Updated: Design of About screen.
- Updated: Design of Licenses dialog.
- Updated: Design of Changelog screen.

### Features
- Added: New screen - Other (contains Feedback, Licenses, Changelog and Privacy Policy screens).
- Added: Link to Learn IT!'s Privacy Policy.
- Added: Support for 18:9 screens.
- Updated: Application rewritten with Kotlin.
- Updated: Slide Navigation changed to Bottom Navigation.
- Updated: Optimized all vector graphics.
- Updated: Minimum SDK to Android 5.1.
- Removed: In-APP donate.
- Removed: Unused resources and texts.
- Removed: Settings screen.

## Learn IT! [2.0.1] - 26.02.2017
### Design
- Updated: Application icon.
- Updated: Overhaul navigation.

### Features
- Added: Support for Android 7.0 and Android 7.1.
- Improved: Images compression.
- Updated: Reduced size of APK.
- Updated: Overhaul performance.

## Learn IT! [2.0] - 28.11.2015
### Design
- Added: New design with Material Design philosophy.

### Features
- Added: Support for Android 6.0.
- Updated: Reduced size of APK.
- Updated: Raster graphics changed to vector.
- Updated: Overhaul navigation.
- Updated: Overhaul performance.

## Learn IT! [1.1.5] - 24.09.2014
### Features
- Added: Licenses dialog.

### Fixes
- Fixed: Colors in About application window.
- Fixed: Crash in Settings window.
- Fixed: Crash during screen orientation changes.
- Fixed: Formulas in articles.
- Fixed: Errors in articles.

## Learn IT! [1.1] - 14.08.2014
### Design
- Updated: Overhaul design.
- Updated: Overhaul navigation.

### Features
- Added: Support for 7-inch devices.
- Updated: Quality of pictures in all articles.
- Updated: Overhaul performance.

### Fixes
- Fixed: Mistakes in localization.

## Learn IT! [1.0.2] - 20.01.2014
### Fixes
- Fixed: Bug, when clicking on any article.
- Fixed: Mistakes in localization.
