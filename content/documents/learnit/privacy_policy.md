---
title: "Privacy Policy"
draft: false
url: "/learnit/privacy_policy/"
---

The following information applies to Learn IT!

**Name of the controller** - Pavel Rekun  
**Address** - Keletskya Street 126a, Ukraine, Vinnytsya   
**Email** - pavel.rekun.development@gmail.com

## Data collection
Learn IT! gathers anonymous usage data by using Firebase. Learn IT! does not collect any personal information such as email addresses, names, addresses, phones, etc.

### What does Firebase record?
Each time the application crashes (i.e. force closes) your device collects anonymous data, which helps resolving the problem. This data includes only stack traces from all threads.

Additionally, Firebase counts the number of active users and how often they use the app. No other information is collected.

### How do we use the collected information?
The tracking information allows to better understand the kind of people who use the app and optimize its functionality and user experience to their needs. Crash reports provided by Firebase are used to improve the stability and performance of the application. No personally identifying data is included.

All of the activity falls within the bounds of the [FIREBASE TERMS OF SERVICE](https://firebase.google.com/terms).
